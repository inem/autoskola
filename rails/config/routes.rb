Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  scope module: :web do
    namespace :learn do
      resources :trainings do
        scope module: :trainings do
          resources :steps, only: [:show, :update]
        end
      end

      root to: "trainings#index"
    end

    namespace :auth do
      # Add a root route if you don't have one...
      # We can use users#new for now, or replace this with the controller and action you want to be the site root:
      root to: "users#new"

      # sign up page with form:
      get 'users/new' => 'users#new', as: :new_user
      post 'users' => 'users#create'

      # log in page with form:
      get '/login'     => 'sessions#new'
      post '/login'    => 'sessions#create'
      delete '/logout' => 'sessions#destroy'
    end
  end

end
