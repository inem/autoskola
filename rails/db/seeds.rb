require_relative '../../lib/text_question'
require_relative '../../lib/text_answer'
require_relative '../../lib/subcategory'
require_relative '../../lib/library'

library = Library.new("../data/subcategories")

puts 'Loading questions...'

library.subcategories.each do |cat|

  puts
  category = Category.find_or_create_by!(
    external_identificator: cat.id
    # text: cat.title
  )

  cat.questions.each do |q|
    question = Category::Question.find_or_create_by!(
      category:               category,
      external_identificator: q.id,
      text:                   q.text.chomp,
      points:                 q.points,
      has_image:              q.has_image,
      answers_required:       q.number_of_choices,
      sort_number:            q.sort_number
    )

    print '.'

    q.answers.each do |a|
      answer = Category::Question::Answer.find_or_create_by!(
          question:               question,
          external_identificator: a.id,
          text:                   a.text.chomp,
          is_correct:             a.is_correct
      )
    end
  end
end

u = User.new(
  name: 'inem@bk.ru',
  email: 'inem@bk.ru',
  password: 'inem@bk.ru',
  password_confirmation: 'inem@bk.ru'
)
u.save!

puts