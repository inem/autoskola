# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180507112953) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categories", force: :cascade do |t|
    t.integer "external_identificator"
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_question_answers", force: :cascade do |t|
    t.integer "question_id"
    t.string "text"
    t.boolean "is_correct"
    t.integer "external_identificator"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_questions", force: :cascade do |t|
    t.integer "points"
    t.string "text"
    t.boolean "has_image"
    t.integer "sort_number"
    t.integer "external_identificator"
    t.integer "answers_required"
    t.integer "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["sort_number"], name: "index_category_questions_on_sort_number"
  end

  create_table "training_step_choices", force: :cascade do |t|
    t.bigint "training_step_id"
    t.bigint "category_question_answer_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_question_answer_id"], name: "index_training_step_choices_on_category_question_answer_id"
    t.index ["training_step_id"], name: "index_training_step_choices_on_training_step_id"
  end

  create_table "training_steps", force: :cascade do |t|
    t.bigint "category_question_id"
    t.bigint "training_id"
    t.integer "number"
    t.boolean "is_correct"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["category_question_id"], name: "index_training_steps_on_category_question_id"
    t.index ["training_id"], name: "index_training_steps_on_training_id"
  end

  create_table "trainings", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_trainings_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "training_step_choices", "category_question_answers"
  add_foreign_key "training_step_choices", "training_steps"
  add_foreign_key "training_steps", "category_questions"
  add_foreign_key "training_steps", "trainings"
  add_foreign_key "trainings", "users"
end
