class CreateTrainingSteps < ActiveRecord::Migration[5.1]
  def change
    create_table :training_steps do |t|
      t.references :category_question, foreign_key: true
      t.references :training, foreign_key: true
      t.integer :number
      t.boolean :is_correct

      t.timestamps
    end
  end
end
