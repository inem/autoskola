class CreateTrainingStepChoices < ActiveRecord::Migration[5.1]
  def change
    create_table :training_step_choices do |t|
      t.references :training_step, foreign_key: true
      t.references :category_question_answer, foreign_key: true

      t.timestamps
    end
  end
end
