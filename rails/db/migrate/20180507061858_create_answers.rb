class CreateAnswers < ActiveRecord::Migration[5.1]
  def change
    create_table :category_question_answers do |t|
      t.integer :question_id
      t.string :text
      t.boolean :is_correct
      t.integer :external_identificator

      t.timestamps
    end
  end
end
