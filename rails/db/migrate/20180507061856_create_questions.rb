class CreateQuestions < ActiveRecord::Migration[5.1]
  def change
    create_table :category_questions do |t|
      t.integer :points
      t.string :text
      t.boolean :has_image
      t.integer :sort_number
      t.integer :external_identificator
      t.integer :answers_required
      t.integer :category_id

      t.timestamps
    end

    add_index :category_questions, :sort_number
  end
end
