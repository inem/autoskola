class Web::ContextController < ApplicationController
  helper_method :current_user

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def authenticate_user!
    if current_user.nil?
      redirect_to auth_login_path, alert: "Please log in first!"
    end
  end
end
