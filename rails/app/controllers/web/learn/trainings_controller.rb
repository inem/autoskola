class Web::Learn::TrainingsController < Web::Learn::ContextController
  def index
    @trainings = current_user.trainings
  end

  def new
    @training = Training.new(user: current_user)

    steps = Category::Question.random(Training::SIZE).each_with_index.map do |q, i|
      Training::Step.new(question: q, number: i + 1)
    end

    @training.steps = steps

    if @training.save
      flash[:alert] = "Start learning!"
      redirect_to learn_training_path(@training)
    else
      flash[:alert] = "Couldn't create new session"
      redirect_to :index
    end
  end

  def show
    @training = Training.find(params[:id])
    redirect_to learn_training_step_path(@training, @training.steps.first.number)
  end
end
