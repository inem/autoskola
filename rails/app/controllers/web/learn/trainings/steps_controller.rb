class Web::Learn::Trainings::StepsController < Web::Learn::Trainings::ContextController
  before_action :step

  def show

  end

  def update
    @step.update(step_params)
    @step.save!

    redirect_to learn_training_step_path(@training, @step.number)
  end

  private

  def step
    @step ||= training.steps.find_by!(number: params[:id])
  end

  def step_params
    params.require(:training_step).permit(:answer_ids, :answer_ids => [])
  end
end
