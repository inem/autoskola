class Web::Learn::Trainings::ContextController < Web::Learn::ContextController
  def training
    @training ||= Training.find(params[:training_id])
  end
end
