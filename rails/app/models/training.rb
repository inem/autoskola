class Training < ApplicationRecord
  belongs_to :user
  has_many :steps

  SIZE = 20

end
