class Training::Step < ApplicationRecord
  belongs_to :question, class_name: "Category::Question", foreign_key: :category_question_id
  belongs_to :training
  has_many :choices, foreign_key: :training_step_id
  has_many :answers, through: :choices, class_name: "Category::Question::Answer", primary_key: :category_question_answer_id

  # scope :answered, -> { where("TRUE") }
  # scope :unanswered, -> { where("TRUE") }

  def next_step_number
    number + 1 if number != Training::SIZE
  end

  def prev_step_number
    number - 1 if number != 0
  end

  def all_answers
    question.answers
  end

  def answered?
    choices.any?
  end

  def number_of_answers_required
    question.answers_required
  end

  def text
    question.text
  end

  def has_image
    question.has_image
  end

  def options
    question.answers
  end
end
