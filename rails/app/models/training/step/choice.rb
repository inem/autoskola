class Training::Step::Choice < ApplicationRecord
  belongs_to :step, foreign_key: :training_step_id
  belongs_to :answer, class_name: "Category::Question::Answer", foreign_key: :category_question_answer_id
end
