class Category::Question < ApplicationRecord
  include QuestionsRepository

  belongs_to :category
  has_many :answers
end
