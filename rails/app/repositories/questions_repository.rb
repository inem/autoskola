module QuestionsRepository
  extend ActiveSupport::Concern

  included do
    scope :complexity1, -> { where(points: 1) }
    scope :complexity2, -> { where(points: 2) }
    scope :complexity3, -> { where(points: 3) }

    scope :has_image, -> { where(has_image: true) }

    scope :random, -> (n) {order("RANDOM()").limit(n) }
  end
end