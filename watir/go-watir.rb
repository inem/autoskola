require 'rubygems'
require 'bundler/setup'
require 'watir'

require_relative 'questions'

def wait_for(b, link)
  until b.url.match(link)
    sleep 0.5
  end
end

def init(login, pwd, entry_page = "http://servisi.euprava.gov.rs/autoskole/prijava")
  Watir.default_timeout = 30000000
  b = Watir::Browser.start entry_page

  b.text_field(id: 'IdNumber').set(login)
  b.text_field(id: 'Password').set(pwd)

  b.button(value: /Пријава кандидата/).click
  b
end

questions_link = "http://servisi.euprava.gov.rs/autoskole/QuestionsPractice/Index/4df0bab4-449e-4c7b-8888-478760167df5"

checked_text_js = <<STR
  return $('input:checked').siblings('label').text()
STR

selected_text_js = <<STR
  return $('.qOptCont.rightAnswer').children('label').text()
STR

global_with_mistakes = []
already_asked = []

lets_play = true
questions_to_learn = (QUESTIONS - EXCLUDE - already_asked)

if questions_to_learn.any?
  b = init(ARGV[0], ARGV[1])
  b.goto(questions_link)

  while lets_play
    correctly_answered = []
    questions_to_learn = (QUESTIONS - EXCLUDE - already_asked)

    puts "Осталось вопросов: #{questions_to_learn.size}"

    batch = questions_to_learn.shuffle.first(25)

    puts "Поехали фигачить еще #{ batch.size } вопросов"
    puts

    print "Без ошибок: "

    batch.each do |question|
      b.radio(value: "2").set
      b.text_field(id: 'QuestionNumber').set(question)
      b.link(visible_text: /Приказ/).click

      already_asked << question

      until b.execute_script("return $('.qOptCont.rightAnswer')").any?
        sleep 0.3
      end
      # require 'pry'; binding.pry

      checked_text = b.execute_script(checked_text_js)
      selected_text = b.execute_script(selected_text_js)

      if checked_text == selected_text
        correctly_answered << question
        print "#{question}, "
      end

      wait_for(b, questions_link)
    end
    puts

    with_mistakes = batch - correctly_answered
    global_with_mistakes += with_mistakes
    puts "Ошибки были в вопросах: #{ with_mistakes*', ' }"

    puts "Eще? 1 - да, 2 - нет"
    lets_play = gets.to_i == 1 ? true : false
  end

  puts
  puts "Вот эти вроде выучил:"
  puts (already_asked - global_with_mistakes) * ', '
else
  puts "Все выучено! Дай пять, чо! 🙌"
end


