class TextQuestion
  attr_reader :points, :text, :has_image, :id, :number_of_choices, :sort_number, :answers
  def initialize(hash)
    @points             = hash.fetch "Points"
    @text               = hash.fetch "Text"
    @has_image          = hash.fetch "HasImage"
    @id                 = hash.fetch "qId"
    @number_of_choices  = hash.fetch "ChoicesReq"
    @sort_number        = hash.fetch "qcId"
    @answers            = []

    hash.fetch("Choices").each do |choice|
      @answers << TextAnswer.new(choice)
    end
  end
end