class Library
  attr_reader :subcategories, :questions3, :questions2, :questions1
  def initialize(path)
    subcategories = []
    Dir.entries(path).each do |filename|
      next unless filename.match('.rb')

      subcategories << Subcategory[filename]
    end

    @subcategories = subcategories

    @questions3 = subcategories.map {|s| s.questions }.flatten.select{|q| q.points == 3}
    @questions2 = subcategories.map {|s| s.questions }.flatten.select{|q| q.points == 2}
    @questions1 = subcategories.map {|s| s.questions }.flatten.select{|q| q.points == 1}
  end
end

