class TextAnswer
  attr_reader :text, :is_correct, :id
  def initialize(hash)
    @is_correct         = hash.fetch "isCorrect"
    @text               = hash.fetch "Text"
    @id                 = hash.fetch "paId"
  end
end
