class Subcategory
  attr_reader :question_numbers, :questions, :id

  def self.[](id)
    require_relative "../data/subcategories/#{id}"
    new($questions_hash, id)
  end

  def initialize(hash, id)
    @id = id
    @questions = hash.map{|q| TextQuestion.new(q)}
    @question_numbers = hash.map{|q| q["qId"]}
  end
end
