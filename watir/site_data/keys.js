function disableKeyNav() {
  $(document).on("keydown", function (n) {
    try {
      if (n.keyCode == 8) return n.preventDefault(), !1
    } catch (t) {}
  });
  $("#btnPrevQ, #btnNextQ, #btnFinishExam, #btnQuestionList, #btnBack").on("keydown", function (n) {
    try {
      if (n.keyCode == 13) return n.preventDefault(), !1
    } catch (t) {}
  })
}

function inputHandlers() {
  if (typeof practiceData != "undefined" && practiceData != null && practiceData.length > 0)
    for (var n = 0; n < practiceData.length; n++) $("#qListQ_" + n).click({
      qIndex: n
    }, function (n) {
      return n.preventDefault(), GoToQuestion(n.data.qIndex, !1, !1), !1
    }), $("#qMarkedLink_" + n).click({
      qIndex: n
    }, function (n) {
      return n.preventDefault(), GoToQuestion(n.data.qIndex, !1, !1), !1
    })
}

function Init() {
  var n = GetPracticeData();
  if (n != "OK") {
    $("#examPracticeMessageContainer").html(n);
    return
  }
  GenerateQuestionsFrame();
  organizeCommandsInit();
  organizeQHeaderInit();
  $("#examPracticeMessageContainer").css("display", "none");
  GoToQuestion(CurrentQuestion, !1, !0)
}

function GetPracticeData() {
  return qPracticeType == 1 ? GetPracticeDataForQSubcateg() : qPracticeType == 2 ? GetPracticeDataForQNumber() : "Unknown practice type selection!"
}

function GetPracticeDataForQSubcateg() {
  var n = "";
  try {
    $.ajax({
      url: _AppDir_ + "/QuestionsPractice/GetQuestionsPracticeData",
      type: "POST",
      async: !1,
      timeout: 12e3,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: "{'id':'" + practiceId + "','languageId':'" + languageId + "','questionSubcategoryId':'" + questionSubcategoryId + "'}"
    }).done(function (t) {
      t.status == "OK" && typeof t.practiceData != "undefined" ? (practiceData = t.practiceData, n = "OK") : n = t.message
    }).fail(function () {
      n = $("#message_GetPracticalDataFail").val()
    }).always(function () {})
  } catch (t) {
    n = $("#message_GetPracticalDataFail").val()
  }
  return n
}

function GetPracticeDataForQNumber() {
  var n = "";
  try {
    $.ajax({
      url: _AppDir_ + "/QuestionsPractice/GetQuestionPracticeData",
      type: "POST",
      async: !1,
      timeout: 12e3,
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      data: "{'id':'" + practiceId + "','languageId':'" + languageId + "','questionNumber':'" + questionNumber + "'}"
    }).done(function (t) {
      t.status == "OK" && typeof t.practiceData != "undefined" ? (practiceData = t.practiceData, n = "OK") : n = t.message
    }).fail(function () {
      n = $("#message_GetPracticalDataFail").val()
    }).always(function () {})
  } catch (t) {
    n = $("#message_GetPracticalDataFail").val()
  }
  return n
}

function GenerateQuestionsFrame() {
  var r, n, t, i;
  if (typeof practiceData != "undefined" && practiceData != null && !(practiceData.length < 1)) {
    for (r = 1, n = 0; n < practiceData.length; n++) $("<div id='questionContainer_" + practiceData[n].qcId + "' class='questionContainer' style='display: none'><\/div>").insertBefore("#examNavigationBar"), $("<label id='qText_" + practiceData[n].qcId + "'><\/label>").appendTo("#questionContainer_" + practiceData[n].qcId), practiceData[n].ChoicesReq > 1 && ($("<br />").appendTo("#questionContainer_" + practiceData[n].qcId), $("<label style='font-style: italic; color: #2c6aa0'>" + $("#message_ChoicesRequired").val() + ":&nbsp;" + practiceData[n].ChoicesReq + "<\/label>").appendTo("#questionContainer_" + practiceData[n].qcId)), practiceData[n].HasImage && ($("<div id='qImgCont_" + practiceData[n].qcId + "' style='margin-top: 6px;'><\/div>").appendTo("#questionContainer_" + practiceData[n].qcId), r > 0 && (SetImageLink(n), r--)), $("<div id='choicesContainer_" + practiceData[n].qcId + "' class='choicesContainer'><\/div>").appendTo("#questionContainer_" + practiceData[n].qcId);
    for ($("<div id='questionsListContainer' style='padding-top: 15px; display: none;'><\/div>").insertBefore("#examNavigationBar"), t = "<table class='table table-striped table-bordered table-hover'>", t += "<thead><tr><th>" + $("#message_QuestionsListTableQuestion").val() + "<\/th><th>" + $("#message_NumberOfPoints").val() + "<\/th><th>" + $("#message_QuestionsListTableIsAnswered").val() + "<\/th><th>" + $("#message_QuestionsListTableIsMarked").val() + "<\/th><\/tr><\/thead>", t += "<tbody>", i = 0; i < practiceData.length; i++) t += "<tr>", t += "<td>", t += "<a id='qListQ_" + i + "' href='#'>" + $("#message_Question").val() + " " + (i + 1) + "<\/a>", t += "<\/td>", t += "<td>" + practiceData[i].Points + "<\/td>", t += "<td id='qListAnswered_" + i + "'><\/td>", t += "<td>", t += "<a href='#' id='qMarkedLink_" + i + "' class='qMarkedLink' style='display: none'><i class='icon-bookmark icon-large'><\/i><i class='icon-bookmark-empty icon-large'><\/i><\/a>", t += "<\/td>", t += "<\/tr>";
    t += "<\/tbody>";
    t += "<\/table>";
    $(t).appendTo("#questionsListContainer")
  }
}

function GoNext() {
  GoToQuestion(CurrentQuestion + 1, !0, !1)
}

function GoPrev() {
  GoToQuestion(CurrentQuestion - 1, !0, !1)
}

function GoToQuestion(n, t, i) {
  typeof practiceData == "undefined" || practiceData == null || n < 0 || n > practiceData.length - 1 || (!t || SaveUserInput(n)) && ($("#examPracticeMessageContainer").text(""), $("#examPracticeMessageContainer").css("display", "none"), $("#questionHeader").css("display", ""), $(".questionContainer").css("display", "none"), n < 1 ? $("#prevQContainer").css("display", "none") : $("#prevQContainer").css("display", ""), n >= practiceData.length - 1 ? $("#nextQContainer").css("display", "none") : $("#nextQContainer").css("display", ""), $("#finishContainer").css("display", ""), $("#answerContainer").css("display", "inline"), $("#qListContainer").css("display", ""), $("#questionsListContainer").css("display", "none"), $("#backContainer").css("display", "none"), $("#examNavigationBar").css("display", ""), ClearCurrentQuestion(), SetNextQestion(n), SetImageLink(n), $("#questionContainer_" + practiceData[n].qcId).css("display", ""), n + 1 < practiceData.length && SetImageLink(n + 1), n - 1 >= 0 && SetImageLink(n - 1), CurrentQuestion = n, $("#pageTitle").text($("#message_Question").val() + ": " + (CurrentQuestion + 1) + "/" + practiceData.length), $("#questionPoints").text($("#message_NumberOfPoints").val() + ": " + practiceData[CurrentQuestion].Points), $("#qMarked").prop("checked", practiceData[CurrentQuestion].IsMarked), i ? setTimeout(function () {
    organizeCommands();
    organizeQHeader()
  }, 100) : (organizeCommands(), organizeQHeader()))
}

function GoToList() {
  typeof practiceData != "undefined" && practiceData != null && SaveUserInput(CurrentQuestion) && ($("#questionHeader").css("display", "none"), $(".questionContainer").css("display", "none"), $("#prevQContainer").css("display", "none"), $("#nextQContainer").css("display", "none"), $("#finishContainer").css("display", "none"), $("#qListContainer").css("display", "none"), $("#answerContainer").css("display", "none"), $("#backContainer").css("display", ""), RefreshQuestionList(), $("#questionsListContainer").css("display", ""))
}

function RefreshQuestionList() {
  var n, i, r, t;
  if (typeof practiceData != "undefined" && practiceData != null && practiceData.length > 0)
    for (n = 0; n < practiceData.length; n++) {
      if (i = !1, typeof practiceData[n].Choices != "undefined" && practiceData[n].Choices != null && practiceData[n].Choices.length > 0) {
        for (r = 0, t = 0; t < practiceData[n].Choices.length; t++) practiceData[n].Choices[t].isChosen && r++;
        practiceData[n].ChoicesReq == r && (i = !0)
      }
      i ? $("#qListAnswered_" + n).html("<i class='icon-check'><\/i>") : $("#qListAnswered_" + n).html("<i class='icon-check-empty'><\/i>");
      practiceData[n].IsMarked ? $("#qMarkedLink_" + n).css("display", "") : $("#qMarkedLink_" + n).css("display", "none")
    }
}

function ClearCurrentQuestion() {
  var n = practiceData[CurrentQuestion],
    r = $("#qText_" + n.qcId),
    t, i;
  if (r.text(""), r.hasClass("missing-data") && r.removeClass("missing-data"), $("#lblQuestionChoicesMissing").remove(), n.Choices != null && n.Choices.length > 0)
    for (t = 0; t < n.Choices.length; t++) i = "qChoice_" + n.qcId + "_" + n.Choices[t].paId, $("#lbl_" + i).remove(), $("#" + i).remove(), $("#qOptCont_" + i).remove();
  $("#choicesContainer_" + n.qcId).remove();
  $("#explBody_" + n.qcId).remove();
  $("#explHead_" + n.qcId).remove();
  $("#explCont_" + n.qcId).remove()
}

function SetNextQestion(n) {
  var t = practiceData[n],
    u = $("#qText_" + t.qcId),
    r, i, f, e;
  if (t.Text == null || trim(t.Text) == "" ? (u.text($("#message_QuestionTextMissing").val()), u.hasClass("missing-data") || u.addClass("missing-data")) : (u.text(t.Text), u.hasClass("missing-data") && u.removeClass("missing-data")), adaptQImage(n), $("<div id='choicesContainer_" + t.qcId + "' class='choicesContainer'><\/div>").appendTo("#questionContainer_" + t.qcId), t.Choices == null || t.Choices.length < 1) $("<label id='lblQuestionChoicesMissing' class='missing-data'>" + $("#message_QuestionChoicesMissing").val() + "<\/label>").appendTo("#choicesContainer_" + t.qcId);
  else
    for (r = 0; r < t.Choices.length; r++) i = "qChoice_" + t.qcId + "_" + t.Choices[r].paId, $("<div id='qOptCont_" + i + "' class='qOptCont'><\/div>").appendTo("#choicesContainer_" + t.qcId), f = "", f = t.ChoicesReq > 1 ? "checkbox" : "radio", e = "", t.Choices[r].isChosen && (e = "checked='checked'"), $("<input type='" + f + "' id='" + i + "' name='qChoice_" + t.qcId + "' class='qOptCont_Choice' value='" + t.Choices[r].paId + "' " + e + " />").appendTo("#qOptCont_" + i), t.ChoicesReq > 1 && $("input[type='checkbox'][name='qChoice_" + t.qcId + "']").change({
      qcId: t.qcId,
      choicesReq: t.ChoicesReq
    }, function (n) {
      $("input[type='checkbox'][name='qChoice_" + n.data.qcId + "']:checked").length > n.data.choicesReq && $(this).removeAttr("checked")
    }), t.Choices[r].Text == null || trim(t.Choices[r].Text) == "" ? $("<label id='lbl_" + i + "' for='" + i + "' class='missing-data qOptCont_Label'>" + $("#message_QuestionChoiceTextMissing").val() + "<\/label>").appendTo("#qOptCont_" + i) : $("<label id='lbl_" + i + "' for='" + i + "' class='qOptCont_Label'>" + t.Choices[r].Text + "<\/label>").appendTo("#qOptCont_" + i), $("<br />").appendTo("#choicesContainer_" + t.qcId)
}

function SetImageLink(n) {
  practiceData[n].HasImage && $("#qImage_" + practiceData[n].qcId).length < 1 && $("<img id='qImage_" + practiceData[n].qcId + "' src='" + _AppDir_ + "/Question/QuestionsPracticeImage?id=" + practiceData[n].qId + "&guid=" + $("#practiceId").val() + "' alt='" + $("#message_QuestionImageAlt").val() + "' style='border: 1px solid gray; padding: 5px; max-height: 402px; max-width: 100%;' onerror='reloadQuestionImage(" + practiceData[n].qcId + ", " + practiceData[n].qId + ");' />").appendTo("#qImgCont_" + practiceData[n].qcId)
}

function adaptQImage(n) {
  var i, r, t;
  practiceData[n].HasImage && $("#qImage_" + practiceData[n].qcId).length > 0 && (i = $(window).height(), (typeof i == "undefined" || i == null) && (i = 0), r = 422, t = i - r, (typeof t == "undefined" || t == null || t < 200) && (t = 200), $("#qImage_" + practiceData[n].qcId).css("max-height", t + "px"))
}

function reloadQuestionImage(n, t) {
  if (!($("#btnRefresh_" + n).length > 0)) {
    var i = $("#qImage_" + n),
      r = 0;
    i.hasClass("qRetry2") ? r = 2 : i.hasClass("qRetry1") && (r = 1);
    r < 2 ? (r += 1, i.addClass("qRetry" + r), setTimeout(function () {
      i.attr("src", _AppDir_ + "/Question/QuestionsPracticeImage?id=" + t + "&guid=" + $("#practiceId").val() + "&t=" + (new Date).getTime())
    }, 1e3)) : (i.unbind("error"), $("<br /><a id='btnRefresh_" + n + "' href='#'><i class='icon-repeat'><\/i>&nbsp;&nbsp;" + $("#message_ReloadQuestionImage").val() + "<\/a>").insertAfter("#qImgCont_" + n), $("#btnRefresh_" + n).click(function (n) {
      return n.preventDefault(), i.attr("src", _AppDir_ + "/Question/QuestionsPracticeImage?id=" + t + "&guid=" + $("#practiceId").val() + "&t=" + (new Date).getTime()), !1
    }))
  }
}

function SaveUserInput(n) {
  var i, r, t, u;
  try {
    if (practiceData[CurrentQuestion].Choices == null || practiceData[CurrentQuestion].Choices.length < 1) return alertify.alert($("#message_AnswerNotSaved").val() + " " + $("#message_QuestionChoicesMissing").val(), function () {
      GoToQuestion(n, !1, !1)
    }), $(".alertify-message").addClass("error-message"), !1;
    if (i = $("input:checked[name='qChoice_" + practiceData[CurrentQuestion].qcId + "']"), i.length > 0 && practiceData[CurrentQuestion].ChoicesReq != i.length) return alertify.alert($("#message_IncorrectNumberOfAnswers").val()), $(".alertify-message").addClass("error-message"), !1;
    for (r = !0, t = 0, t = 0; t < practiceData[CurrentQuestion].Choices.length; t++) u = i.filter("input:checked[id='qChoice_" + practiceData[CurrentQuestion].qcId + "_" + practiceData[CurrentQuestion].Choices[t].paId + "']").length > 0, practiceData[CurrentQuestion].Choices[t].isChosen = u, practiceData[CurrentQuestion].Choices[t].isCorrect != u && (r = !1);
    return i.length < 1 ? !0 : r ? !0 : ShowAnswer() ? (alertify.alert($("#message_WrongAnswer").val()), $(".alertify-message").addClass("error-message"), !1) : !0
  } catch (f) {
    return alertify.alert($("#message_SaveAnswerError").val()), $(".alertify-message").addClass("error-message"), !1
  }
}

function ShowAnswer() {
  var n, t;
  if ($("#explCont_" + practiceData[CurrentQuestion].qcId).length > 0) return !1;
  for (choiceIndex = 0; choiceIndex < practiceData[CurrentQuestion].Choices.length; choiceIndex++) n = $("#qOptCont_qChoice_" + practiceData[CurrentQuestion].qcId + "_" + practiceData[CurrentQuestion].Choices[choiceIndex].paId), practiceData[CurrentQuestion].Choices[choiceIndex].isCorrect ? (n.hasClass("wrongAnswer") && n.removeClass("wrongAnswer"), n.hasClass("rightAnswer") || n.addClass("rightAnswer")) : (n.hasClass("rightAnswer") && n.removeClass("rightAnswer"), n.hasClass("wrongAnswer") || n.addClass("wrongAnswer"));
  return $("<div id='explCont_" + practiceData[CurrentQuestion].qcId + "' class='explCont'><\/div>").appendTo("#questionContainer_" + practiceData[CurrentQuestion].qcId), $("<div id='explHead_" + practiceData[CurrentQuestion].qcId + "' class='explHead'><\/div>").appendTo("#explCont_" + practiceData[CurrentQuestion].qcId), $("<div id='explBody_" + practiceData[CurrentQuestion].qcId + "' class='explBody'><\/div>").appendTo("#explCont_" + practiceData[CurrentQuestion].qcId), t = practiceData[CurrentQuestion].Explanation, (t == null || trim(t) == "") && (t = $("#message_ExplanationMissing").val()), $("#explHead_" + practiceData[CurrentQuestion].qcId).append($("#message_Explanation").val()), $("#explBody_" + practiceData[CurrentQuestion].qcId).append(t), findMaxWidthOption(), resizeOptions(), !0
}

function findMaxWidthOption() {
  optMaxWidth = 0;
  optMaxWidthId = "";
  var n = 0;
  $("label[id^='lbl_qChoice_']").each(function () {
    n = $(this).width();
    n > optMaxWidth && (optMaxWidth = n, optMaxWidthId = $(this).attr("id"))
  })
}

function resizeOptions() {
  if (!($("#explCont_" + practiceData[CurrentQuestion].qcId).length < 1) && optMaxWidthId != "") {
    var n = $("#" + optMaxWidthId).width();
    $("label[id^='lbl_qChoice_']").each(function () {
      $(this).attr("id") != optMaxWidthId && $(this).width(n)
    })
  }
}

function organizeCommandsInit() {
  _examNavigationBar = $("#examNavigationBar");
  _btnPrevQ = $("#btnPrevQ");
  _btnNextQ = $("#btnNextQ");
  _btnFinishExam = $("#btnFinishExam");
  _btnQuestionList = $("#btnQuestionList");
  _btnAnswer = $("#btnAnswer");
  _prevQContainer = $("#prevQContainer");
  _nextQContainer = $("#nextQContainer");
  _finishContainer = $("#finishContainer");
  _qListContainer = $("#qListContainer");
  _answerContainer = $("#answerContainer");
  _fltCl = $("#fltCl");
  _fltCl2 = $("#fltCl2");
  _fltCl3 = $("#fltCl3");
  _fltCl4 = $("#fltCl4")
}

function organizeCommands() {
  var t, i, n;
  (_examNavigationBar.css("text-align", "center"), _btnPrevQ.css("width", "auto"), _btnNextQ.css("width", "auto"), _btnFinishExam.css("width", "auto"), _btnQuestionList.css("width", "auto"), _btnAnswer.css("width", "auto"), _prevQContainer.css("float", "left"), _fltCl4.css("clear", "none"), _nextQContainer.css("float", "left"), _fltCl3.css("clear", "none"), _finishContainer.css("float", "right"), _fltCl2.css("clear", "none"), _qListContainer.css("float", "right"), _fltCl.css("clear", "none"), _answerContainer.css("margin-left", "auto"), _answerContainer.css("margin-right", "auto"), _answerContainer.css("margin-top", "0px"), _answerContainer.css("margin-bottom", "0px"), _answerContainer.css("text-align", "left"), _answerContainer.css("display") != "none" && _answerContainer.css("display", "inline"), t = 0, _prevQContainer.css("display") != "none" ? t = _btnPrevQ.position().top : _nextQContainer.css("display") != "none" ? t = _btnNextQ.position().top : _finishContainer.css("display") != "none" ? t = _btnFinishExam.position().top : _qListContainer.css("display") != "none" && (t = _btnQuestionList.position().top), t != 0) && (_answerContainer.css("display") != "none" && _btnAnswer.position().top != t && (_examNavigationBar.css("text-align", "left"), _fltCl.css("clear", "both"), _answerContainer.css("margin-left", "0px"), _answerContainer.css("margin-right", "0px"), _answerContainer.css("margin-top", "0px"), _answerContainer.css("margin-bottom", "0px"), _answerContainer.css("display") != "none" && _answerContainer.css("display", "")), _qListContainer.css("display") != "none" && _btnQuestionList.position().top != t && (_examNavigationBar.css("text-align", "left"), _fltCl2.css("clear", "both"), _qListContainer.css("float", "none")), _finishContainer.css("display") != "none" && _btnFinishExam.position().top != t && (_examNavigationBar.css("text-align", "left"), _fltCl3.css("clear", "both"), _finishContainer.css("float", "none")), i = !1, _prevQContainer.css("display") == "none" && _nextQContainer.css("display") != "none" && _finishContainer.css("display") != "none" && _btnFinishExam.position().top != t && (i = !0), _prevQContainer.css("display") != "none" && _nextQContainer.css("display") == "none" && _finishContainer.css("display") != "none" && _btnFinishExam.position().top != t && (i = !0), _nextQContainer.css("display") != "none" && _btnNextQ.position().top != t && (i = !0), i && (_examNavigationBar.css("text-align", "left"), _finishContainer.css("display") != "none" && (_fltCl3.css("clear", "both"), _finishContainer.css("float", "none")), _nextQContainer.css("display") != "none" && (_fltCl4.css("clear", "both"), _nextQContainer.css("float", "none")), _prevQContainer.css("display") != "none" && _prevQContainer.css("float", "none")), n = 0, _prevQContainer.css("display") != "none" && (n = _btnPrevQ.width()), _nextQContainer.css("display") != "none" && _nextQContainer.css("float") == "none" && _btnNextQ.width() > n && (n = _btnNextQ.width()), _finishContainer.css("display") != "none" && _finishContainer.css("float") == "none" && _btnFinishExam.width() > n && (n = _btnFinishExam.width()), _qListContainer.css("display") != "none" && _qListContainer.css("float") == "none" && _btnQuestionList.width() > n && (n = _btnQuestionList.width()), _answerContainer.css("display") != "none" && _btnAnswer.position().top != t && _btnAnswer.width() > n && (n = _btnAnswer.width()), n > 0 && (_prevQContainer.css("display") != "none" && _prevQContainer.css("float") == "none" && _btnPrevQ.width(n), _nextQContainer.css("display") != "none" && _nextQContainer.css("float") == "none" && _btnNextQ.width(n), _finishContainer.css("display") != "none" && _finishContainer.css("float") == "none" && _btnFinishExam.width(n), _qListContainer.css("display") != "none" && _qListContainer.css("float") == "none" && _btnQuestionList.width(n), _answerContainer.css("display") != "none" && _btnAnswer.position().top != t && _btnAnswer.width(n)))
}

function organizeQHeaderInit() {
  _questionHeader = $("#questionHeader");
  _qHeaderContainer = $("#qHeaderContainer");
  _pageTitle = $("#pageTitle");
  _timerCountdown = $("#timerCountdown");
  _qMarkingContainer = $("#qMarkingContainer");
  _questionMarking = $("#questionMarking");
  _hdrFltCl = $("#hdrFltCl");
  _hdrFltCl2 = $("#hdrFltCl2")
}

function organizeQHeader() {
  if (_questionHeader.css("display") != "none") {
    _qHeaderContainer.css("text-align", "center");
    _hdrFltCl2.css("clear", "none");
    _timerCountdown.css("float", "right");
    _hdrFltCl.css("clear", "none");
    _qMarkingContainer.css("margin-left", "auto");
    _qMarkingContainer.css("margin-right", "auto");
    _qMarkingContainer.css("margin-top", "0px");
    _qMarkingContainer.css("margin-bottom", "0px");
    _qMarkingContainer.css("text-align", "left");
    _qMarkingContainer.css("display") != "none" && _qMarkingContainer.css("display", "inline");
    var n = 0;
    _pageTitle.css("display") != "none" && (n = _pageTitle.position().top);
    _questionMarking.css("display") != "none" && _questionMarking.position().top != n && (_qHeaderContainer.css("text-align", "left"), _hdrFltCl.css("clear", "both"), _qMarkingContainer.css("margin-left", "0px"), _qMarkingContainer.css("margin-right", "0px"), _qMarkingContainer.css("margin-top", "0px"), _qMarkingContainer.css("margin-bottom", "0px"), _qMarkingContainer.css("display") != "none" && _qMarkingContainer.css("display", ""));
    _timerCountdown.length > 0 && _timerCountdown.css("display") != "none" && _timerCountdown.position().top != n && (_qHeaderContainer.css("text-align", "left"), _hdrFltCl2.css("clear", "both"), _timerCountdown.css("float", "none"))
  }
}

function trim(n) {
  return n.replace(/^\s+|\s+$/g, "")
}

function ltrim(n) {
  return n.replace(/^\s+/, "")
}

function rtrim(n) {
  return n.replace(/\s+$/, "")
}
var optMaxWidth, optMaxWidthId;
$(document).ready(function () {
  Init();
  disableKeyNav();
  $("#btnPrevQ").click(function (n) {
    return n.preventDefault(), GoPrev(), !1
  });
  $("#btnNextQ").click(function (n) {
    return n.preventDefault(), GoNext(), !1
  });
  $("#btnQuestionList").click(function (n) {
    return n.preventDefault(), GoToList(), !1
  });
  $("#btnAnswer").click(function (n) {
    return n.preventDefault(), ShowAnswer(), !1
  });
  $("#btnBack").click(function (n) {
    return n.preventDefault(), GoToQuestion(CurrentQuestion, !1, !1), !1
  });
  $("#qMarked").change(function () {
    practiceData[CurrentQuestion].IsMarked = $("#qMarked").is(":checked")
  });
  inputHandlers();
  $(window).resize(function () {
    adaptQImage(CurrentQuestion);
    resizeOptions();
    organizeCommands();
    organizeQHeader()
  });
  KeepSessionAliveAndWell(sessionInterval);
  qPracticeType == 2 && ($("#questionHeader").css("display", "none"), $("#qListContainer").css("display", "none"))
});
var practiceData = null,
  CurrentQuestion = 0,
  isExamFinished = !1;
optMaxWidth = 0;
optMaxWidthId = "";
var _examNavigationBar = null,
  _btnPrevQ = null,
  _btnNextQ = null,
  _btnFinishExam = null,
  _btnQuestionList = null,
  _btnAnswer = null,
  _prevQContainer = null,
  _nextQContainer = null,
  _finishContainer = null,
  _qListContainer = null,
  _answerContainer = null,
  _fltCl = null,
  _fltCl2 = null,
  _fltCl3 = null,
  _fltCl4 = null;
var _questionHeader = null,
  _qHeaderContainer = null,
  _pageTitle = null,
  _timerCountdown = null,
  _qMarkingContainer = null,
  _questionMarking = null,
  _hdrFltCl = null,
  _hdrFltCl2 = null