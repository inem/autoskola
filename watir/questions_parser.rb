require 'rubygems'
require 'bundler/setup'
require 'watir'
require 'nokogiri'

require_relative 'questions'

def wait_for(b, link)
  until b.url.match(link)
    sleep 0.5
  end
end

def wait_until_exist(element)
  until element.exists?
    sleep 0.5
  end
end

def init(login, pwd, entry_page = "http://servisi.euprava.gov.rs/autoskole/prijava")
  Watir.default_timeout = 30000000
  b = Watir::Browser.start entry_page

  b.text_field(id: 'IdNumber').set(login)
  b.text_field(id: 'Password').set(pwd)

  b.button(value: /Пријава кандидата/).click
  b
end

questions_link = "http://servisi.euprava.gov.rs/autoskole/QuestionsPractice/Index/4df0bab4-449e-4c7b-8888-478760167df5"

checked_text_js = <<STR
  return $('input:checked').siblings('label').text()
STR

selected_text_js = <<STR
  return $('.qOptCont.rightAnswer').children('label').text()
STR

b = init("150042182100006", "100006")
b.goto(questions_link)

# require 'pry'; binding.pry


# s = b.select_list id: 'QuestionCategoryId'
# s.selected_options

# s = b.select_list id: 'QuestionSubcategoryId'
# s.selected_options

categories = {}

options = b.execute_script("return $('#QuestionCategoryId').html()")
page = Nokogiri::HTML(options)

page.css("option").each do |o|
  id = o.attributes["value"].value
  title = o.content
  categories[id] = title
end

subcategories = categories.dup

parsed = [
  91, 94, 103, 109, 115, 118, 126, 127

]

empty_subcategories = [
 93, 95, 96, 97, 98, 99, 100, 101, 102, 104, 105, 106, 107, 108,
 112, 113, 114, 116, 117, 119, 120, 121, 122,
 123, 124, 125, 128,
]

to_skip = []
added = []

categories.each do |id, text|
  print "| #{text} |"
  s = b.select_list id: 'QuestionCategoryId'
  wait_until_exist(s)
  s.select text.strip
  sleep 5

  # ss = b.select_list id: 'QuestionSubcategoryId'
  # wait_until_exist(ss)

  options = b.execute_script("return $('#QuestionSubcategoryId').html()")
  page = Nokogiri::HTML(options)

  page.css("option").each do |o|
    # prikaz = b.link(visible_text: /Приказ/)
    # wait_until_exist(prikaz)

    print "."

    sub_id = o.attributes["value"].value
    sub_title = o.content

    print sub_id

    # require 'pry'; binding.pry

    next if File.exist?("data/subcategories/#{sub_id}.rb")
    # next if parsed.include?(sub_id.to_i)
    next if empty_subcategories.include?(sub_id.to_i)

    print "->("
    sleep 2

    s = b.select_list id: 'QuestionCategoryId'
    wait_until_exist(s)
    s.select text.strip

    subcategories[id] = [sub_id, sub_title]

    ss = b.select_list id: 'QuestionSubcategoryId'

    if ss.selected_options.map(&:text).first == sub_title
      puts "*"
    else
      # require 'pry'; binding.pry
      ss.select(sub_title.strip)
    end
    sleep 1
    print "."

    b.link(visible_text: /Приказ/).click
    print "Click)"

    sleep 3

    btn = b.button value: 'OK'

    print " ?? "

    if btn.exist?

      btn.click
      puts "Ignore next time: #{sub_id}"
      puts "Clicked OK"
      # sleep 1
      to_skip << sub_id
    else
      practice_data = b.execute_script("return practiceData")
      File.write("data/subcategories/#{sub_id}.rb", practice_data)
      added << sub_id

      puts "Saved #{sub_id}"

      b.link(visible_text: /Излаз/).click

      puts "Clicked Излаз"
      sleep 10
    end
  end
end

at_exit do
  puts "To skip: #{ to_skip * ', ' }"
  puts "Added: #{ added * ', ' }"
end

require 'pry'; binding.pry
