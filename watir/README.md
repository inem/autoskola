## What

CLI tool to speed up the process of going through the list of specific questions of the Official Sebian Traffic Laws Exams simulation.

## Why

The official website is not super functional. Fo example it doesn't collect questions answered with mistakes to go through them later.

However, you can do it by yourself. After each simulation you get a list of incorrectly answered questions:

![](https://i.imgur.com/S1iDX8t.png)

Collect them, add numbers to `questions.rb`, and launch `make run-test login=1xxxxxxxxxx pwd=xxxxxxx` to go through the list of questions

## Prerequisites

You must be registered on [the official site](http://servisi.euprava.gov.rs/autoskole/) to use it.


## Misc

[List of recources used during development](http://tabsqueeze.com/~/pFRF2GbdsCCPfOfiVQLA)