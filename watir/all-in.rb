require 'rubygems'
require 'bundler/setup'
require 'watir'

require_relative 'questions'
require_relative 'lib/question'
require_relative 'lib/subcategory'
require_relative 'lib/library'

library = Library.new("data/subcategories")


# (QUESTIONS - EXCLUDE + library.questions).each do |n|
#   File.write("data/learning/failed/#{n}", "")
# end

def wait_for(b, link)
  until b.url.match(link)
    sleep 0.5
  end
end

def init(login, pwd, entry_page = "http://servisi.euprava.gov.rs/autoskole/prijava")
  Watir.default_timeout = 30000000
  b = Watir::Browser.start entry_page

  b.text_field(id: 'IdNumber').set(login)
  b.text_field(id: 'Password').set(pwd)

  b.button(value: /Пријава кандидата/).click
  b
end

questions_link = "http://servisi.euprava.gov.rs/autoskole/QuestionsPractice/Index/4df0bab4-449e-4c7b-8888-478760167df5"

checked_text_js = <<STR
  return $('input:checked').siblings('label').text()
STR

selected_text_js = <<STR
  return $('.qOptCont.rightAnswer').children('label').text()
STR

global_with_mistakes = []
already_asked = []

lets_play = true
library_questions = []

def get_numbers(slug)
  Dir.entries("data/learning/#{slug}").map do |filename|
    next if [".", ".."].include? filename
    filename.to_i
  end.compact
end

failed = []


library_questions = library.questions2.map(&:id) #get_numbers(:to_learn)
failed_questions = get_numbers(:failed)
learned_questions = get_numbers(:learned)

# questions_to_learn = (library_questions + QUESTIONS - EXCLUDE - learned_questions).uniq
# questions_to_learn = failed_questions + QUESTIONS
questions_to_learn = QUESTIONS# - learned_questions

if questions_to_learn.any?
  b = init(ARGV[0], ARGV[1])
  b.goto(questions_link)

  while lets_play
    correctly_answered = []
    # questions_to_learn = (library_questions + QUESTIONS - EXCLUDE - already_asked - learned_questions).uniq
    # questions_to_learn = failed_questions + QUESTIONS - already_asked
    questions_to_learn = QUESTIONS - already_asked #- learned_questions

    puts "Осталось вопросов: #{questions_to_learn.size}"

    batch = questions_to_learn.shuffle.first(500)

    puts "Поехали фигачить еще #{ batch.size } вопросов"
    puts

    print "Без ошибок: "

    batch.each do |question|
      b.radio(value: "2").set
      b.text_field(id: 'QuestionNumber').set(question)
      b.link(visible_text: /Приказ/).click

      already_asked << question

      b.execute_script 'console.log("----------")'
      b.execute_script '_questionHeader.show();'
      # b.execute_script '$("#btnAnswer").click(function(){ window.location.href = $("#btnFinishExam").attr("href")  })'
      b.execute_script '$("#btnAnswer").click(function(){ $("#explCont_1").remove()  })'

      until b.execute_script("return $('.qOptCont.rightAnswer')").any?
        sleep 0.3
      end
      # require 'pry'; binding.pry

      checked_text = b.execute_script(checked_text_js)
      selected_text = b.execute_script(selected_text_js)

      if checked_text == selected_text
        correctly_answered << question
        File.write("data/learning/learned/#{question}", "")
        print "#{question}, "
      else
        File.write("data/learning/failed/#{question}", "")
      end

      wait_for(b, questions_link)
    end
    puts

    with_mistakes = batch - correctly_answered
    global_with_mistakes += with_mistakes
    puts "Ошибки были в вопросах: #{ with_mistakes*', ' }"


    puts "Eще? 1 - да, 2 - нет"
    lets_play = gets.to_i == 1 ? true : false
  end

  puts
  puts "Вот эти вроде выучил:"
  puts (already_asked - global_with_mistakes) * ', '
else
  puts "Все выучено! Дай пять, чо! 🙌"
end


