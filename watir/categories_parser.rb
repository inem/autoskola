require 'rubygems'
require 'bundler/setup'
require 'watir'
require 'nokogiri'

require_relative 'questions'

def wait_for(b, link)
  until b.url.match(link)
    sleep 0.5
  end
end

def init(login, pwd, entry_page = "http://servisi.euprava.gov.rs/autoskole/prijava")
  Watir.default_timeout = 30000000
  b = Watir::Browser.start entry_page

  b.text_field(id: 'IdNumber').set(login)
  b.text_field(id: 'Password').set(pwd)

  b.button(value: /Пријава кандидата/).click
  b
end

questions_link = "http://servisi.euprava.gov.rs/autoskole/QuestionsPractice/Index/4df0bab4-449e-4c7b-8888-478760167df5"

checked_text_js = <<STR
  return $('input:checked').siblings('label').text()
STR

selected_text_js = <<STR
  return $('.qOptCont.rightAnswer').children('label').text()
STR

b = init("150042182100006", "100006")
b.goto(questions_link)

# require 'pry'; binding.pry


# s = b.select_list id: 'QuestionCategoryId'
# s.selected_options

# s = b.select_list id: 'QuestionSubcategoryId'
# s.selected_options

categories = {}

options = b.execute_script("return $('#QuestionCategoryId').html()")
page = Nokogiri::HTML(options)

page.css("option").each do |o|
  id = o.attributes["value"].value
  title = o.content
  categories[id] = title
end

subcategories = categories.dup

categories.each do |id, text|
  s = b.select_list id: 'QuestionCategoryId'
  s.select text
  sleep 5

  options = b.execute_script("return $('#QuestionSubcategoryId').html()")
  page = Nokogiri::HTML(options)

  page.css("option").each do |o|
    sub_id = o.attributes["value"].value
    sub_title = o.content
    subcategories[id] = [sub_id, sub_title]
  end
end

require 'pry'; binding.pry